#!/usr/bin/env python
from __future__ import print_function

import argparse
import email.utils
import logging
import os.path
import random
import re
import subprocess
import sys
import tarfile
import tempfile
import zipfile
from HTMLParser import HTMLParser

import chardet
import lxml.etree as ET


INDEX = 'index.html'
RDF_NS = u'http://www.w3.org/1999/02/22-rdf-syntax-ns#'
MAFF_NS = u'http://maf.mozdev.org/metadata/rdf#'
NC_NS = u'http://home.netscape.com/NC-rdf#'
ET.register_namespace('RDF', RDF_NS)
ET.register_namespace('MAFF', MAFF_NS)
ET.register_namespace('NC', NC_NS)


class IndexURLParser(HTMLParser):
    URL_RE = re.compile("""http[s]?://
                        (?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|
                        (?:%[0-9a-fA-F][0-9a-fA-F]))+
                        """, re.VERBOSE)

    def __init__(self, infile_str=""):
        HTMLParser.__init__(self)
        self.url = None
        self.title = None
        self.encoding = 'utf8'
        self.__title_parsing = False

        with open(infile_str, 'r') as infile:
                instr = infile.read()
                try:
                    instr = instr.decode(self.encoding)
                except UnicodeDecodeError:
                    instr_enc = chardet.detect(instr)
                    self.encoding = instr_enc['encoding']
                    instr = instr.decode(self.encoding)
                self.feed(instr)
        logging.debug('self.encoding = %s', self.encoding)

    def handle_comment(self, data):
        data = data.strip()
        urls = self.URL_RE.findall(data)
        if urls:
            self.url = urls[0]

    def handle_starttag(self, tag, attrs):
        if tag == 'title':
            self.__title_parsing = True

    def handle_data(self, data):
        if self.__title_parsing:
            self.title = data.strip()
            self.__title_parsing = False


def generate_RDF(index_time, index_file):
    # <?xml version="1.0" encoding="UTF-8"?>
    # <rdf:RDF
    #    xmlns:ns1="http://maf.mozdev.org/metadata/rdf#"
    #    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" >
    #   <rdf:Description rdf:nodeID="Nc90b8576ad194aab9cab0b223a185328">
    #     <ns1:archivetime rdf:resource="2005-11-10T14:59:37Z"/>
    #     <ns1:charset rdf:resource="utf-8"/>
    #     <ns1:originalurl
    #       rdf:resource="http://maf.mozdev.org/maff-specification.html"/>
    #     <ns1:title rdf:resource="maff"/>
    #     <ns1:indexfilename rdf:resource="index.html"/>
    #   </rdf:Description>
    # </rdf:RDF>
    url_parser = IndexURLParser(index_file)

    root = ET.Element('{%s}RDF' % RDF_NS)
    desc = ET.SubElement(root, '{%s}Description' % RDF_NS,
                         attrib={'{%s}about' % RDF_NS: 'urn:root'})
    if url_parser.url is not None:
        ET.SubElement(desc, '{%s}originalurl' % MAFF_NS,
                      attrib={'{%s}resource' % RDF_NS: url_parser.url})
    if url_parser.title is not None:
        ET.SubElement(desc, '{%s}title' % MAFF_NS,
                      attrib={'{%s}resource' % RDF_NS: url_parser.title})
    ET.SubElement(desc, '{%s}archivetime' % MAFF_NS,
                  attrib={'{%s}resource' % RDF_NS:
                          email.utils.formatdate(index_time)})
    ET.SubElement(desc, '{%s}indexfilename' % MAFF_NS,
                  attrib={'{%s}resource' % RDF_NS: u'index.html'})
    ET.SubElement(desc, '{%s}charset' % MAFF_NS,
                  attrib={'{%s}resource' % RDF_NS: url_parser.encoding})

    return ET.tostring(root, encoding='utf8', xml_declaration=True,
                       pretty_print=True)


def process_war_file(infilename, outfilename):
    print("Processing ... %s" % infilename, file=sys.stderr)
    tempdir = tempfile.mkdtemp(prefix='war2maff')
    tempdir_len = len(tempdir) + 1
    inner_dir = "%018s" % str(random.randint(0, sys.maxsize))[:18]

    try:
        tarball = tarfile.TarFile.gzopen(infilename)
        # It is NOT possible that it could be called anything else
        # than 'index.html' ... file is created by Konqueror
        try:
            index_info = tarball.getmember(INDEX)
        except KeyError:
            # Missing index.html ... just give up
            raise tarfile.TarError("Missing index.html file.")
        time = index_info.mtime
        tarball.extractall(tempdir)
        index_file = os.path.join(tempdir, INDEX)
        RDF_string = generate_RDF(time, index_file)

        with zipfile.ZipFile(outfilename,
                             mode='w',
                             compression=zipfile.ZIP_DEFLATED,
                             allowZip64=True) as zf:
            for this_dir, _, files in os.walk(tempdir):
                logging.debug('this_dir = {}, files = {}'.format(
                    this_dir, files))
                for f in files:
                    f_name = os.path.join(this_dir, f)
                    arc_name = os.path.normpath(
                        os.path.join(inner_dir, f_name[tempdir_len:]))
                    logging.debug('f_name = {}, arc_name = {}'.format(
                        f_name, arc_name))
                    zf.write(f_name, arcname=arc_name)

            zf.writestr(os.path.join(inner_dir, 'index.rdf'), RDF_string)

        tarball.close()
    except tarfile.TarError:
        pass

    finally:
        subprocess.check_call(['rm', '-rf', tempdir])


if __name__ == '__main__':
    arg_parse = argparse.ArgumentParser()
    arg_parse.add_argument('infile', help='source .war archive')
    arg_parse.add_argument('outfile', nargs='?',
                           help='resulting .maff archive')
    arg_parse.add_argument('-d', '--debug', action='store_true',
                           help='debugging output')
    args = arg_parse.parse_args()
    if args.outfile is None:
        args.outfile = os.path.splitext(args.infile)[0] + '.maff'

    if args.debug:
        logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s',
                            level=logging.DEBUG)
    else:
        logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s',
                            level=logging.INFO)

    process_war_file(args.infile, args.outfile)
